let f_princ = 261.62;
let f_princ_reg = 4;
let f_first_note = f_princ / Math.pow(2, f_princ_reg);
let f_last_note = Math.pow(2, (83 - f_princ_reg * 12) / 12) * f_princ;
let value_0 = Math.log(f_first_note) / Math.log(f_last_note);

function Frequency_to_percentage(frequency) {
  var value = Math.log(frequency) / Math.log(f_last_note);

  var linear = (value - value_0) / (1 - value_0);
  return linear;
}

url = ["", "test.nzc"];
if (url != null) {
  Load_file_at_URL(url);
}

function Draw_minimap_layer(
  time_array,
  percentual_freq_array,
  timeSignature,  
) {
  var canvas = document.querySelector(".previewMinimapCanvas");

  if (canvas == null) {
    return;
  }

  var minimapContainer = document.querySelector(".previewMinimapContainer");
 

  minimapContainer.style.width = timeSignature;

  canvas.height = canvas.offsetHeight;
  canvas.width = timeSignature;
  var ctx = canvas.getContext("2d");
  for (i = 0; i < percentual_freq_array.length; i++) {
    for (j = 0; j <= percentual_freq_array[i].length; j++) {
      ctx.beginPath();
      ctx.moveTo(
        Math.ceil(time_array[i][j] * 18),
        Math.round((1 - percentual_freq_array[i][j]) * canvas.offsetHeight)
      );
      ctx.lineTo(
        Math.ceil(time_array[i][j + 1] * 18),
        Math.round((1 - percentual_freq_array[i][j]) * canvas.offsetHeight)
      );
      ctx.strokeStyle = "#43433B";
      ctx.stroke();
    }
  }
}

var locate = window.location.toString();
function Delineate_file_from_url(str) {
  var position_url = str.search("FileURL=");
  var position_file = str.search("Filename=");
  var position_nzc = str.search(".nzc");
  if (position_url != -1 && position_file != -1 && position_nzc != -1) {
    return [
      str.substring(position_url + 8, position_file - 1),
      str.substring(position_file + 9, position_nzc + 4),
    ];
  } else {
    return null;
  }
}

var url = Delineate_file_from_url(locate);

function Load_file_at_URL(url) {
  var request = new XMLHttpRequest();
  var adress = url[0] + url[1];
  var timeData = [];
  var freqData = [];
  var timeSig = 0;

  request.open("GET", adress, true);
  request.send(null);
  request.onreadystatechange = function () {
    if (request.readyState === 4 && request.status === 200) {
      var type = request.getResponseHeader("Content-Type");
      if (type.indexOf("text") !== 1) {
        var returnValue = request.responseText;
        var datatest = JSON.parse(returnValue);
        timeSig =
          (datatest.global_variables.Li / datatest.global_variables.PPM) *
          60 *
          18;

        datatest.voice_data.forEach((voice) => {
          voice.data.midi_data.time.push(
            (60 * datatest.global_variables.Li) / datatest.global_variables.PPM
          );
          timeData.push(voice.data.midi_data.time);

          freqData.push(
            voice.data.midi_data.note_freq.map((f) => {
              return Frequency_to_percentage(f);
            })
          );
        });
        console.log(datatest);

        Draw_minimap_layer(timeData, freqData, timeSig, url[1]);
      }
    }
  };
}
